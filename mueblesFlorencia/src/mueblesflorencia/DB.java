package mueblesflorencia;

import java.util.ArrayList;

public class DB {
    
    ArrayList<Clientes> clientes(){
        ArrayList<Clientes> listadoCLientes = new ArrayList();
        //Clientes
        Clientes miCliente1 = new Clientes();
        miCliente1.nombre = "Pedro";
        
        Clientes miCliente2 = new Clientes();
        miCliente2.nombre = "Luisa";
        
        Clientes miCliente3 = new Clientes();
        miCliente3.nombre = "Ariel";
        
        Clientes miCliente4 = new Clientes();
        miCliente4.nombre = "El descreido";
        
        listadoCLientes.add(miCliente3);
        listadoCLientes.add(miCliente2);
        listadoCLientes.add(miCliente1);
        listadoCLientes.add(miCliente4);
        return listadoCLientes;
    }
    
    ArrayList<Muebles> muebles(){
        ArrayList<Muebles> listadoMuebles = new ArrayList<>();
        //Muebles
        Muebles miMueble1 = new Muebles();
        miMueble1.nombre = "cama";
        miMueble1.precio = 100;
        
        Muebles miMueble2 = new Muebles();
        miMueble2.nombre = "silla";
        miMueble2.precio = 40;
        
        Muebles miMueble3 = new Muebles();
        miMueble3.nombre = "mesa";
        miMueble3.precio = 70;
        
        listadoMuebles.add(miMueble3);
        listadoMuebles.add(miMueble2);
        listadoMuebles.add(miMueble1);
    
        
        return listadoMuebles;
    }
    
}
