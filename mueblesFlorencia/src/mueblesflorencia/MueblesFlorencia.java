package mueblesflorencia;

import java.util.ArrayList;
import java.util.Date;

public class MueblesFlorencia {

    public static void main(String[] args) {
        System.out.println("Muebleria La Florencia");        
        //Nuestra "base de datos"
        ArrayList<Clientes> misClientes = new DB().clientes();        
        ArrayList<Muebles> misMuebles = new DB().muebles();        
                        
        //Reservas
        Reserva miReserva1 = new Reserva();
        miReserva1.elClienteQueLoQuiera = misClientes.get(3);
        miReserva1.elMuebleElegido = misMuebles.get(0);        
        System.out.println(miReserva1);
    }
    
}
